/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2018-2021 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdlib.h>
#include <string.h>
#include <sysctlinfo.h>

#include "sysctlmibinfo2.h"

/* from <sysutils/sysctlbyname-improved> */
#ifndef OBJIDEXTENDED_BYNAME
#define OBJIDEXTENDED_BYNAME 10
#endif

/* Internals */
static struct sysctlmif_object *
alloc_object(int mode, char *buf, bool init_children, int idnext[CTL_MAXNAME],
    size_t *idnextlevel, char *nextname)
{
	struct sysctlmif_object *object;
	char *namep, *descp, *fmtp, *labelp, *nextnamep;
	size_t idlevel;
	int *idp, *idnextp, error;
	unsigned int kind;

	switch(mode) {
	case SEROBJ:
	case SEROBJ_BYNAME:
		SYSCTLINFO_DESOBJ(buf, idlevel, idp, namep, descp, kind, fmtp,
		    labelp);
		break;
	case SEROBJNEXTNODE:
	case SEROBJNEXTLEAF:
		SYSCTLINFO_DESOBJ_NEXTOID(buf, idlevel, idp, namep, descp, kind,
		    fmtp, labelp, *idnextlevel, idnextp);
		memcpy(idnext, idnextp, *idnextlevel * sizeof(int));
		break;
	case SEROBJNEXTNODE_BYNAME:
	case SEROBJNEXTLEAF_BYNAME:
		SYSCTLINFO_DESOBJ_NEXTNAME(buf, idlevel, idp, namep, descp,
		    kind, fmtp, labelp, nextnamep);
		strcpy(nextname, nextnamep);
	}

	if ((object = malloc(sizeof(struct sysctlmif_object))) == NULL)
		return NULL;
	memset(object, 0, sizeof(struct sysctlmif_object));

	error = 0;
	error += ((object->name  = strdup(namep))  == NULL);
	error += ((object->desc  = strdup(descp))  == NULL);
	error += ((object->fmt   = strdup(fmtp))   == NULL);
	error += ((object->label = strdup(labelp)) == NULL);
	object->id = (int*)malloc(sizeof(int) * idlevel);
	if (error > 0 || object->id == NULL) {
		sysctlmif_freeobject(object);
		return (NULL);
	}
	memcpy(object->id, idp, sizeof(int) * idlevel);
	object->idlevel = idlevel;
	object->type  = SYSCTLMIF_KINDTYPE(kind);
	object->flags = SYSCTLMIF_KINDFLAGS(kind);

	if (init_children) {
		object->children = malloc(sizeof(struct sysctlmif_list));
		if (object->children == NULL) {
			sysctlmif_freeobject(object);
			return (NULL);
		}
		SLIST_INIT(object->children);
	}
    
	return object;
}

static struct sysctlmif_object *
new_object_byname(int mode, const char *name, bool init_children, char *nextname)
{
	struct sysctlmif_object *object = NULL;
	char *buf;
	size_t buflen = 0;
	int prop[2];
    
	prop[0] = CTL_SYSCTL;
	prop[1] = mode;

	if (SYSCTLINFO_BYNAME(name, prop, NULL, &buflen) != 0)
		return NULL;
	if ((buf = (char*)malloc(buflen)) == NULL)
		return NULL;
	if (SYSCTLINFO_BYNAME(name, prop, buf, &buflen) != 0)
		return NULL;

	object = alloc_object(mode, buf, init_children, NULL, 0, nextname);
	free(buf);
    
	return object;
}

static struct sysctlmif_object *
new_object(int mode, int *id, size_t idlevel, bool init_children,
    int idnext[CTL_MAXNAME], size_t *idnextlevel)
{
	struct sysctlmif_object *object = NULL;
	char *buf;
	size_t buflen = 0;
	int prop[2];
    
	prop[0] = CTL_SYSCTL;
	prop[1] = mode;

	if (SYSCTLINFO(id, idlevel, prop, NULL, &buflen) != 0)
		return NULL;
	if ((buf = (char*)malloc(buflen)) == NULL)
		return NULL;
	if (SYSCTLINFO(id, idlevel, prop, buf, &buflen) != 0)
		return NULL;

	object = alloc_object(mode, buf, init_children, idnext, idnextlevel, NULL);
	free(buf);
    
	return object;
}

/* Depth-First Traversal: grouplist() or leaves() */
static struct sysctlmif_list *
dft_list(int mode, int *idstart, size_t idstartlen, int *size)
{
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME];
	size_t i, idlevel, idnextlevel;
	struct sysctlmif_list *list = NULL;
	struct sysctlmif_object *last, *new;

	*size = 0;

	if ((list = malloc(sizeof(struct sysctlmif_list))) == NULL)
		return (NULL);
	SLIST_INIT(list);

	memcpy(id, idstart, idstartlen * sizeof(int));
	idlevel = idstartlen;

	new = new_object(mode, id, idlevel, false, idnext, &idnextlevel);
	if (new == NULL) {
		free(list);
		return (NULL);
	}
	SLIST_INSERT_HEAD(list, new, object_link);
	*size = 1;

	for (;;) {
		memcpy(id, idnext, idnextlevel * sizeof(int));
		idlevel = idnextlevel;

		if (idlevel < idstartlen)
			break;

		for (i = 0; i < idstartlen; i++) {
			if (id[i] != idstart[i])
				return (list);
		}

		last = new;
		idnextlevel = CTL_MAXNAME;
		new = new_object(mode, id, idlevel, false, idnext, &idnextlevel);
		if (new == NULL) {
			sysctlmif_freelist(list);
			return (NULL);
		}
		SLIST_INSERT_AFTER(last, new, object_link);
		*size = *size + 1;
	}

	return (list);
}

/* return a list of trees with id/idlevel and its brothers */
static struct sysctlmif_list *brothers(int *id, size_t idlevel)
{
	struct sysctlmif_list *list, *pathlist[CTL_MAXNAME + 1];
	struct sysctlmif_object *first, *child, *pathobj[CTL_MAXNAME + 1];
	int childid[CTL_MAXNAME], idnext[CTL_MAXNAME];
	size_t childidlevel, idnextlevel;

	if ((list = malloc(sizeof(struct sysctlmif_list))) == NULL)
		return (NULL);
	SLIST_INIT(list);
	pathlist[idlevel-1] = list;

	first = new_object(SEROBJNEXTNODE, id, idlevel, true, idnext, &idnextlevel);
	if (first == NULL)
		return NULL;
	SLIST_INSERT_HEAD(list, first, object_link);

	pathlist[idlevel] = first->children;
	pathobj[idlevel] = first;
	while(idnextlevel >= idlevel) {
		memcpy(childid, idnext, idnextlevel * sizeof(int));
		childidlevel = idnextlevel;
		child = new_object(SEROBJNEXTNODE, childid, childidlevel, true,
		    idnext, &idnextlevel);
		if (child == NULL) {
			sysctlmif_freemib(list);
			return NULL;
		}

		if (SLIST_EMPTY(pathlist[childidlevel-1]))
			SLIST_INSERT_HEAD(pathlist[childidlevel-1], child, object_link);
		else
			SLIST_INSERT_AFTER(pathobj[childidlevel], child, object_link);

		pathlist[childidlevel] = child->children;
		pathobj[childidlevel] = child;
	}

	return list;
}

/* API implementation */

int sysctlmif_name(int *id, size_t idlevel, char *name, size_t *namelen)
{
	int prop[2] = {CTL_SYSCTL, OBJNAME};

	return (SYSCTLINFO(id, idlevel, prop, (void *)name, namelen));
}

int sysctlmif_oidbyname(const char *name, int *id, size_t *idlevel)
{
	int error, prop[2] = {CTL_SYSCTL, OBJID_BYNAME};
	size_t tmplevel = *idlevel * sizeof(int);

	if ((error = SYSCTLINFO_BYNAME(name, prop, id, &tmplevel)) == 0)
		*idlevel = tmplevel / sizeof(int);
	
	return (error);
}

int sysctlmif_oidextendedbyname(const char *name, int *id, size_t *idlevel)
{
	int error, prop[2] = {CTL_SYSCTL, OBJIDEXTENDED_BYNAME};
	size_t tmplevel = *idlevel * sizeof(int);
	
	if ((error = SYSCTLINFO_BYNAME(name, prop, id, &tmplevel)) == 0)
		*idlevel = tmplevel / sizeof(int);
	
	return (error);
}

/* alias for sysctlmif_oidextendedbyname(), compatibility <= 2.0.0 */
int sysctlmif_oidinputbyname(const char *name, int *id, size_t *idlevel)
{

	return (sysctlmif_oidextendedbyname(name, id, idlevel));
}

int sysctlmif_desc(int *id, size_t idlevel, char *desc, size_t *desclen)
{
	int prop[2] = {CTL_SYSCTL, OBJDESC};

	return (SYSCTLINFO(id, idlevel, prop, (void *)desc, desclen));
}

int sysctlmif_descbyname(const char *name, char *desc, size_t *desclen)
{
	int prop[2] = {CTL_SYSCTL, OBJDESC_BYNAME};

	return (SYSCTLINFO_BYNAME(name, prop, (void *)desc, desclen));
}

int sysctlmif_label(int *id, size_t idlevel, char *label, size_t *labellen)
{
	int prop[2] = {CTL_SYSCTL, OBJLABEL};

	return (SYSCTLINFO(id, idlevel, prop, (void *)label, labellen));
}

int sysctlmif_labelbyname(const char *name, char *label, size_t *labellen)
{
	int prop[2] = {CTL_SYSCTL, OBJLABEL_BYNAME};

	return (SYSCTLINFO_BYNAME(name, prop, (void *)label, labellen));
}

int sysctlmif_hashandler(int *id, size_t idlevel, bool *hashandler)
{
	int prop[2] = {CTL_SYSCTL, OBJHASHANDLER};
	size_t bool_size = sizeof(bool);

	return (SYSCTLINFO(id, idlevel, prop, (void *)hashandler, &bool_size));
}

int sysctlmif_hashandlerbyname(const char *name, bool *hashandler)
{
	int prop[2] = {CTL_SYSCTL, OBJHASHANDLER_BYNAME};
	size_t bool_size = sizeof(bool);

	return (SYSCTLINFO_BYNAME(name, prop, (void *)hashandler, &bool_size));
}

int sysctlmif_kind(int *id, size_t idlevel, unsigned int *kind)
{
	int prop[2] = {CTL_SYSCTL, OBJKIND};
	size_t u_int_size = sizeof(unsigned int);

	return (SYSCTLINFO(id, idlevel, prop, (void *)kind, &u_int_size));
}

int sysctlmif_kindbyname(const char *name, unsigned int *kind)
{
	int prop[2] = {CTL_SYSCTL, OBJKIND_BYNAME};
	size_t u_int_size = sizeof(unsigned int);

	return (SYSCTLINFO_BYNAME(name, prop, (void *)kind, &u_int_size));
}

int sysctlmif_fmt(int *id, size_t idlevel, char *fmt, size_t *fmtlen)
{
	int prop[2] = {CTL_SYSCTL, OBJFMT};

	return (SYSCTLINFO(id, idlevel, prop, (void *)fmt, fmtlen));
}

int sysctlmif_fmtbyname(const char *name, char *fmt, size_t *fmtlen)
{
	int prop[2] = {CTL_SYSCTL, OBJFMT_BYNAME};

	return (SYSCTLINFO_BYNAME(name, prop, (void *)fmt, fmtlen));
}

int sysctlmif_nextnode(int *id, size_t idlevel, int *idnext, size_t *idnextlevel)
{
	int error, prop[2] = {CTL_SYSCTL, NEXTOBJNODE};
	size_t tmp_nextlevel = *idnextlevel * sizeof(int);

	if ((error = SYSCTLINFO(id, idlevel, prop, idnext, &tmp_nextlevel)) == 0)
		*idnextlevel = tmp_nextlevel / sizeof(int);

	return (error);
}

int sysctlmif_nextnodebyname(const char *name, char *next, size_t *nextlen)
{
	int prop[2] = {CTL_SYSCTL, NEXTOBJNODE_BYNAME};

	return (SYSCTLINFO_BYNAME(name, prop, next, nextlen));
}

int sysctlmif_nextleaf(int *id, size_t idlevel, int *idnext, size_t *idnextlevel)
{
	int error, prop[2] = {CTL_SYSCTL, NEXTOBJLEAF};
	size_t tmp_nextlevel = *idnextlevel * sizeof(int);

	if ((error = SYSCTLINFO(id, idlevel, prop, idnext, &tmp_nextlevel)) == 0)
		*idnextlevel = tmp_nextlevel / sizeof(int);

	return (error);
}

int sysctlmif_nextleafbyname(const char *name, char *next, size_t *nextlen)
{
	int prop[2] = {CTL_SYSCTL, NEXTOBJLEAF_BYNAME};

	return (SYSCTLINFO_BYNAME(name, prop, next, nextlen));
}

/* struct sysctlmif_object API */

struct sysctlmif_object *sysctlmif_object(int *id, size_t idlevel)
{

	return (new_object(SEROBJ, id, idlevel, false, NULL, 0));
}

struct sysctlmif_object *sysctlmif_objectbyname(const char *name)
{

	return (new_object_byname(SEROBJ_BYNAME, name, false, NULL));
}

void sysctlmif_freeobject(struct sysctlmif_object *object)
{
	if (object == NULL)
		return;

	free(object->id);
	free(object->name);
	free(object->desc);
	free(object->label);
	free(object->fmt);
	free(object);
	object = NULL;
}

struct sysctlmif_list *sysctlmif_list()
{
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME];
	size_t idlevel, idnextlevel;
	struct sysctlmif_list *list = NULL;
	struct sysctlmif_object *last, *new;

	if ((list = malloc(sizeof(struct sysctlmif_list))) == NULL)
		return (NULL);
	SLIST_INIT(list);

	id[0] = 0;
	idlevel = 1;
	new = new_object(SEROBJNEXTNODE, id, idlevel, false, idnext, &idnextlevel);
	if (new == NULL) {
		free(list);
		return NULL;
	}
	SLIST_INSERT_HEAD(list, new, object_link);

	while (idnextlevel != 0) {
		last = new;
		memcpy(id, idnext, idnextlevel * sizeof(int));
		idlevel = idnextlevel;

		new = new_object(SEROBJNEXTNODE, id, idlevel, false, idnext,
		    &idnextlevel);
		if (new == NULL) {
			sysctlmif_freelist(list);
			return (NULL);
		}
		SLIST_INSERT_AFTER(last, new, object_link);
	}

	return (list);
}

struct sysctlmif_list *sysctlmif_grouplist(int *idroot, size_t idrootlevel)
{
	int unused;

	return (dft_list(SEROBJNEXTNODE, idroot, idrootlevel, &unused));
}

struct sysctlmif_list *sysctlmif_grouplistbyname(const char *name)
{
	int idroot[CTL_MAXNAME], unused;
	size_t idrootlevel = CTL_MAXNAME;

	if (sysctlmif_oidbyname(name, idroot, &idrootlevel) != 0)
		return (NULL);

	return (dft_list(SEROBJNEXTNODE, idroot, idrootlevel, &unused));
}

struct sysctlmif_list *sysctlmif_leaves(int *idroot, size_t idrootlevel)
{
	int size;
	struct sysctlmif_list *list;
	struct sysctlmif_object *root;

	if ((list = dft_list(SEROBJNEXTLEAF, idroot, idrootlevel, &size)) == NULL)
		return NULL;
	
	if (size > 1) {
		root = SLIST_FIRST(list);
		SLIST_REMOVE_HEAD(list, object_link);
		sysctlmif_freeobject(root);
	}
	
	return (list);
}

struct sysctlmif_list *sysctlmif_leavesbyname(const char *name)
{
	int idroot[CTL_MAXNAME];
	size_t idrootlevel = CTL_MAXNAME;

	if (sysctlmif_oidbyname(name, idroot, &idrootlevel) != 0)
		return (NULL);

	return (sysctlmif_leaves(idroot, idrootlevel));
}

void sysctlmif_freelist(struct sysctlmif_list *list)
{
	struct sysctlmif_object *obj;

	if (list == NULL)
		return;

	while (!SLIST_EMPTY(list)) {
		obj = SLIST_FIRST(list);
		SLIST_REMOVE_HEAD(list, object_link);
		sysctlmif_freeobject(obj);
	}

	free(list);
	list = NULL;
}

struct sysctlmif_object *sysctlmif_tree(int *idroot, size_t idrootlevel)
{
	struct sysctlmif_object *root;
	struct sysctlmif_list *children;
	int idnext[CTL_MAXNAME];
	size_t idnextlevel;

	root = new_object(SEROBJNEXTNODE, idroot, idrootlevel, true, idnext,
	    &idnextlevel);
	if (root == NULL)
		return NULL;

	if(idnextlevel > idrootlevel) {
		if((children = brothers(idnext, idnextlevel)) == NULL) {
			sysctlmif_freeobject(root);
			return NULL;
		}
		free(root->children);
		root->children = children;
	}

	return (root);
}

struct sysctlmif_object *sysctlmif_treebyname(const char *name)
{
	int idroot[CTL_MAXNAME];
	size_t idrootlevel = CTL_MAXNAME;

	if (sysctlmif_oidbyname(name, idroot, &idrootlevel) != 0)
		return (NULL);

	return (sysctlmif_tree(idroot, idrootlevel));
}

void sysctlmif_freetree(struct sysctlmif_object *root)
{
	struct sysctlmif_object *child;

	if (root == NULL)
		return;
	if (root->children == NULL) {
		sysctlmif_freeobject(root);
		return;
	}

	while (!SLIST_EMPTY(root->children)) {
		child = SLIST_FIRST(root->children);
		SLIST_REMOVE_HEAD(root->children, object_link);
		sysctlmif_freetree(child);
	}
	sysctlmif_freeobject(root);
}

struct sysctlmif_list *sysctlmif_mib()
{
	int id[1] = {0};

	return (brothers(id, 1));
}

void sysctlmif_freemib(struct sysctlmif_list *mib)
{
	struct sysctlmif_object *root;
	    
	if (mib == NULL)
		return;

	while (!SLIST_EMPTY(mib)) {
		root = SLIST_FIRST(mib);
		SLIST_REMOVE_HEAD(mib, object_link);
		sysctlmif_freetree(root);
	}

	free(mib);
	mib = NULL;
}
