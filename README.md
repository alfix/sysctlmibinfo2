sysctlmibinfo2 ver. 2.0.1
=========================

Index

 1. [Introduction](#introduction)
 2. [Description](#description)
 3. [Getting started](#getting-started)
 4. [API](#api)
 5. [Manual](#manual)
 6. [Examples](#examples)
 7. [Real-World Use Cases](#real-world-use-cases)
 8. [Off-Topic](off-topic)
 9. [Extras](#extras)

Introduction
-----

The sysctlmibinfo2 library provides an API to explore the
[FreeBSD](https://www.freebsd.org) sysctl MIB and to get the info of an object,
therefore it is useful to handle an object correctly and to build a
[sysctl](https://www.freebsd.org/doc/handbook/configtuning-sysctl.html)-like
utility.
The library depends on [sysctlinfo](https://gitlab.com/alfix/sysctlinfo) and
[sysctlbyname-improved](https://gitlab.com/alfix/sysctlbyname-improved),
the legacy [version 1](https://gitlab.com/alfix/sysctlmibinfo) depends on the
undocumented interface implemented in kern/kern\_sysctl.c.

Description
-----------

FreeBSD Quarterly Status Report
[sysctlmibinfo2 API](https://www.freebsd.org/news/status/report-2020-01-2020-03.html#sysctlmibinfo2-API).

Getting Started
---------------

To install the port
[devel/libsysctlmibinfo2](https://www.freshports.org/devel/libsysctlmibinfo2/):
	
	# cd /usr/ports/devel/libsysctlmibinfo2/ && make install clean

To add the package:

	# pkg install libsysctlmibinfo2


Example to implement "/sbin/sysctl -d hw.snd":
```c
#include <stdio.h>
#include <sysctlmibinfo2.h>

int main()
{
	struct sysctlmif_list *list;
	struct sysctlmif_object *obj;

	if ((list = sysctlmif_grouplistbyname("hw.snd")) == NULL)
		return (1);

	SLIST_FOREACH(obj, list, object_link)
		printf("%s: %s\n", obj->name, obj->desc);

	sysctlmif_freelist(list);

	return (0);
}
```
```
% cc -I/usr/local/include example1.c -L/usr/local/lib -lsysctlmibinfo2 -o example1
```

Example to implement "/sbin/sysctl -aN":
```c
#include <sys/param.h>

#include <stdio.h>
#include <string.h>
#include <sysctlmibinfo2.h>

int main()
{
        char name[MAXPATHLEN], next[MAXPATHLEN];
        size_t nextlen = MAXPATHLEN;

        strcpy(next, "kern");
	do {
                strncpy(name, next, nextlen);
                printf("%s\n", name);
                nextlen = MAXPATHLEN;
        } while(sysctlmif_nextnodebyname(name, next, &nextlen) == 0);

        return (0);
}
```
```
% cc -I/usr/local/include example2.c -L/usr/local/lib -lsysctlmibinfo2 -o example2
```

API
---

Primarily sysctlmibinfo2 wraps [sysctlinfo](https://gitlab.com/alfix/sysctlinfo)
and [sysctlbyname-improved](https://gitlab.com/alfix/sysctlbyname-improved)
to provide an easy C API to explore the sysctl MIB-Tree and to get the info of
an object.

```c
/* object info */
int sysctlmif_name(int *id, size_t idlevel, char *name, size_t *namelen);
int sysctlmif_oidbyname(const char *name, int *id, size_t *idlevel);
int sysctlmif_oidextendedbyname(const char *name, int *id, size_t *idlevel);
int sysctlmif_desc(int *id, size_t idlevel, char *desc, size_t *desclen);
int sysctlmif_descbyname(const char *name, char *desc, size_t *desclen);
int sysctlmif_label(int *id, size_t idlevel, char *label, size_t *labellen);
int sysctlmif_labelbyname(const char *name, char *label, size_t *labellen);
int sysctlmif_fmt(int *id, size_t idlevel, char *fmt, size_t *fmtlen);
int sysctlmif_fmtbyname(const char *name, char *fmt, size_t *fmtlen);
int sysctlmif_hashandler(int *id, size_t idlevel, bool *hashandler);
int sysctlmif_hashandlerbyname(const char *name, bool *hashandler);
int sysctlmif_kind(int *id, size_t idlevel, unsigned int *kind);
int sysctlmif_kindbyname(const char *name, unsigned int *kind);
unsigned int SYSCTLMIF_KINDTYPE(unsigned int kind);
unsigned int SYSCTLMIF_KINDFLAGS(unsigned int kind);
/* explore MIB-Tree */
int sysctlmif_nextnode(int *id, size_t idlevel, int *idnext, size_t *idnextlevel);
int sysctlmif_nextnodebyname(const char *name, char *next, size_t *namelen);
int sysctlmif_nextleaf(int *id, size_t idlevel, int *idnext, size_t *idnextlevel);
int sysctlmif_nextleafbyname(const char *name, char *next, size_t *namelen);
```

Moreover sysctlmibinfo2 provides a high level API: a *struct sysctlmif\_object*
definition and functions to build data structures of objects.

```c
SLIST_HEAD(sysctlmif_list, sysctlmif_object);

struct sysctlmif_object {
	SLIST_ENTRY(sysctlmif_object) object_link;
	int      *id;            /* array of idlevel entries  */
	size_t   idlevel;        /* between 1 and CTL_MAXNAME */
	char     *name;          /* name in MIB notation      */
	char     *desc;          /* description               */
	char     *label;         /* aggregation label         */
	uint8_t  type;           /* defined in <sys/sysctl.h> */
	uint32_t flags;          /* defined in <sys/sysctl.h> */
	char     *fmt;           /* format string             */
	struct sysctlmif_list *children; /* children list     */
};

/* single object*/
struct sysctlmif_object *sysctlmif_object(int *id, size_t idlevel);
struct sysctlmif_object *sysctlmif_objectbyname(const char *name);
void sysctlmif_freeobject(struct sysctlmif_object *object);
/* list */
struct sysctlmif_list *sysctlmif_list();
struct sysctlmif_list *sysctlmif_grouplist(int *id, size_t idlevel);
struct sysctlmif_list *sysctlmif_grouplistbyname(const char *name);
struct sysctlmif_list *sysctlmif_leaves(int *id, size_t idlevel);
struct sysctlmif_list *sysctlmif_leavesbyname(const char *name);
void sysctlmif_freelist(struct sysctlmif_list *list);
/* tree */
struct sysctlmif_object *sysctlmif_tree(int *id, size_t idlevel);
struct sysctlmif_object *sysctlmif_treebyname(const char *name);
void sysctlmif_freetree(struct sysctlmif_object *object_root);
/* MIB */
struct sysctlmif_list *sysctlmif_mib();
void sysctlmif_freemib(struct sysctlmif_list *mib);
```

Manual
------

[sysctlmibinfo2(3)](https://alfonsosiciliano.gitlab.io/posts/2019-12-09-manual-sysctlmibinfo2.html).

Examples
--------

[Examples](https://gitlab.com/alfix/sysctlmibinfo2/tree/master/examples)
in the _Public Domain_ to build new projects:

 - [wrappers.c](examples/wrappers.c) - Useful for getting one property
 - [object.c](examples/object.c) - For getting all the properties
 - [list.c](examples/list.c) - All objects in a list
 - [grouplist.c](examples/grouplist.c) - Objects list of a Depth First Traversal
 - [leaves.c](examples/leaves.c) - Descendant leaves list of an object
 - [tree.c](examples/tree.c) - Subtree
 - [mib.c](examples/mib.c) - Complete MIB

Real-World Use Cases
--------------------

 - [sysctlview](https://gitlab.com/alfix/sysctlview)
 - [mixertui](https://gitlab.com/alfix/mixertui)
 - [nsysctl](https://gitlab.com/alfix/nsysctl)
 - [sysctl-mib-html](https://gitlab.com/alfix/sysctl-mib-html)

Off-Topic
---------

get/set an object value [sysctl(3)](https://man.freebsd.org/sysctl/3), example
for a sysctlmif\_object:
```c
sysctl(object->id, object->idlevel, oldp, oldplen, newp, newplen);
```

add/remove an object [sysctl(9)](https://man.freebsd.org/sysctl/9),
Capability mode [cap\_enter(2)](https://man.freebsd.org/cap_enter/2).

Extras
------

 * mailing list: http://lists.freebsd.org/pipermail/freebsd-hackers/2018-December/053767.html
