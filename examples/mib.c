/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written in 2021 by Alfonso Sabato Siciliano.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <stdio.h>
#include <sysctlmibinfo2.h>

/* preorder visit */
void show_tree(struct sysctlmif_object *node)
{
	struct sysctlmif_object *child;

	printf("%s\n", node->name);

	if (node->children == NULL)
		return;

	SLIST_FOREACH(child, node->children, object_link)
		show_tree(child);
}

/* Complete sysctl MIB-Tree */
int main()
{
	struct sysctlmif_list *mib;
	struct sysctlmif_object *root;

	printf("\n---------- sysctl MIB ----------\n");

	if ((mib = sysctlmif_mib()) == NULL)
		return (1);

	SLIST_FOREACH(root, mib, object_link)
		show_tree(root);

	sysctlmif_freemib(mib);

	return (0);
}
