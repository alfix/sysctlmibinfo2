/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written in 2021 by Alfonso Sabato Siciliano.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <stdio.h>
#include <sysctlmibinfo2.h>

/* sysctlmif_grouplist(), Depth First Traversal list */
int main()
{
	struct sysctlmif_list *list;
	struct sysctlmif_object *obj;

	printf("\n---------- vm.stats ----------\n");

	if ((list = sysctlmif_grouplistbyname("vm.stats")) == NULL)
		return (1);

	SLIST_FOREACH(obj, list, object_link)
		printf("%s\n", obj->name);

	sysctlmif_freelist(list);

	return (0);
}
