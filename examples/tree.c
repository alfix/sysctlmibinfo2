/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written in 2021 by Alfonso Sabato Siciliano.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <sysctlmibinfo2.h>

/* preorder visit */
void show_tree(struct sysctlmif_object *node)
{
	struct sysctlmif_object *child;

	printf("%s\n", node->name);

	if (node->children != NULL) {
		SLIST_FOREACH(child, node->children, object_link)
			show_tree(child);
	}
}

/* Subtree examples */
int main()
{
	struct sysctlmif_object *root;;
	int id[CTL_MAXNAME];
	size_t idlevel;

	printf("\n--- vm.stats.* --- \n");

	idlevel = CTL_MAXNAME;
	if (sysctlmif_oidbyname("vm.stats", id, &idlevel) != 0)
		return (1);

	if ((root = sysctlmif_tree(id, idlevel)) == NULL)
		return (1);

	show_tree(root);
	sysctlmif_freetree(root);

	return (0);
}
