/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written in 2021 by Alfonso Sabato Siciliano.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>
#include <sysctlmibinfo2.h>

#define BUFSIZE    1024

/* Traverse the sysctl MIB-Tree printing the info of the objects */
int main()
{
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME];
	int name2id[CTL_MAXNAME];
	char buf[BUFSIZE];
	size_t idlevel, idnextlevel, buflen, name2idlevel, i;
	unsigned int kind;

	id[0] = 0;
	idlevel = 1;
	for (;;) {
		printf("OID: ");
		for (i = 0; i < idlevel; i++)
			printf("%x%c", id[i], i+1 < idlevel ? '.' : '\n');

		/* name */
		if (sysctlmif_name(id, idlevel, NULL, &buflen) != 0)
			return (1);
		if (sysctlmif_name(id, idlevel, buf, &buflen) != 0)
			return (1);
		printf("name [%zuB]: %s\n", buflen, buf);

		/* oidbyname */
		name2idlevel = CTL_MAXNAME;
		if (sysctlmif_oidbyname(buf, name2id, &name2idlevel) != 0)
			return (1);
		printf("OIDbyname: ");
		for (i = 0; i < name2idlevel; i++)
			printf("%x%c", name2id[i], i+1 < name2idlevel ? '.' : '\n');

		/* desc */
		if (sysctlmif_desc(id, idlevel, NULL, &buflen) != 0)
			buflen = 0;
		if (sysctlmif_desc(id, idlevel, buf, &buflen) != 0)
			memset((void *)buf, 0, BUFSIZE);
		printf("descr [%zuB]: %s\n", buflen, buf);

		/* label */
		if (sysctlmif_label(id, idlevel, NULL, &buflen) != 0)
			buflen = 0;
		if (sysctlmif_label(id, idlevel, buf, &buflen) != 0)
			memset((void *)buf, 0, BUFSIZE);
		printf("label [%zuB]: %s\n", buflen, buf);

		/* kind: FLAGS and TYPE */
		if (sysctlmif_kind(id, idlevel, &kind) != 0)
			return (1);
		printf("kind: %u\n", kind);
		printf("   KINDFLAGS(info): %u\n", SYSCTLMIF_KINDFLAGS(kind));
		printf("   KINDTYPE(info):  %u\n", SYSCTLMIF_KINDTYPE(kind));

		/* fmt */
		if (sysctlmif_fmt(id, idlevel, NULL, &buflen) != 0)
			buflen = 0;
		if (sysctlmif_fmt(id, idlevel, buf, &buflen) != 0)
			memset((void *)buf, 0, BUFSIZE);
		printf("fmt [%zuB]: %s\n", buflen, buf);
		
		printf("-------------------------------------------\n");

		/* nextleaf (or nextnode) */
		idnextlevel = CTL_MAXNAME;
		/* if (sysctlmif_nextnode(id, idlevel, idnext, &idnextlevel) !=0) */
		if (sysctlmif_nextleaf(id, idlevel, idnext, &idnextlevel) != 0)
			break;

		memcpy(id, idnext, idnextlevel * sizeof(int));
		idlevel = idnextlevel;
	}

	return (0);
}
