/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written in 2021 by Alfonso Sabato Siciliano.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>
#include <sysctlmibinfo2.h>

/* Explore MIB-Tree, build a sysctlmif_object and print its info */
int main()
{
	struct sysctlmif_object *obj;
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME];
	size_t idlevel, idnextlevel, i;

	id[0] = 0;
	idlevel = 1;
	for (;;) {
		obj = sysctlmif_object(id, idlevel);
		if (obj == NULL) {
			return (1);
		}
		printf("sysctlmif_object_t *obj {\n");
		printf("\t obj->id: ");
		for (i = 0; i < idlevel; i++)
			printf("%x%c", obj->id[i], i+1 < obj->idlevel ? '.' : '\n');
		printf("\t obj->idlevel: %zu\n", obj->idlevel);
		printf("\t obj->name:  %s\n", obj->name);
		printf("\t obj->desc:  %s\n", obj->desc);
		printf("\t obj->label: %s\n", obj->label);
		printf("\t obj->type:  %u\n", obj->type);
		printf("\t obj->flags: %x\n", obj->flags);
		printf("\t obj->fmt:   %s\n", obj->fmt);
		printf("}\n");

		sysctlmif_freeobject(obj);
		printf("-------------------------------------------\n");

		idnextlevel = CTL_MAXNAME;
		/* if (sysctlmif_nextnode(id, idlevel, idnext, &idnextlevel) <0) */
		if (sysctlmif_nextleaf(id, idlevel, idnext, &idnextlevel) < 0)
			break;

		memcpy(id, idnext, idnextlevel * sizeof(int));
		idlevel = idnextlevel;
	}

	return (0);
}
