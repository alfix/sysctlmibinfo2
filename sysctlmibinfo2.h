/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2018-2021 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _SYSCTLMIBINFO_H_
#define _SYSCTLMIBINFO_H_

#include <sys/types.h>
#include <sys/queue.h>

#include <stdbool.h>

#define SYSCTLMIF_VERSION        2

/* sysctlinfo and sysctlbyname_improved wrappers */

/* object info */
int sysctlmif_name(int *id, size_t idlevel, char *name, size_t *namelen);
int sysctlmif_oidbyname(const char *name, int *id, size_t *idlevel);
int sysctlmif_oidextendedbyname(const char *name, int *id, size_t *idlevel);
int sysctlmif_desc(int *id, size_t idlevel, char *desc, size_t *desclen);
int sysctlmif_descbyname(const char *name, char *desc, size_t *desclen);
int sysctlmif_label(int *id, size_t idlevel, char *label, size_t *labellen);
int sysctlmif_labelbyname(const char *name, char *label, size_t *labellen);
int sysctlmif_fmt(int *id, size_t idlevel, char *fmt, size_t *fmtlen);
int sysctlmif_fmtbyname(const char *name, char *fmt, size_t *fmtlen);
int sysctlmif_hashandler(int *id, size_t idlevel, bool *hashandler);
int sysctlmif_hashandlerbyname(const char *name, bool *hashandler);
int sysctlmif_kind(int *id, size_t idlevel, unsigned int *kind);
int sysctlmif_kindbyname(const char *name, unsigned int *kind);
#define SYSCTLMIF_KINDTYPE(kind)        (kind & 0x0000000f)
#define SYSCTLMIF_KINDFLAGS(kind)       (kind & 0xfffffff0)
/* explore MIB-Tree*/
int sysctlmif_nextnode(int *id, size_t idlevel, int *idnext, size_t *idnextlevel);
int sysctlmif_nextnodebyname(const char *name, char *next, size_t *nextlen);
int sysctlmif_nextleaf(int *id, size_t idlevel, int *idnext, size_t *idnextlevel);
int sysctlmif_nextleafbyname(const char *name, char *next, size_t *nextlen);
/* compatibility <= 2.0.0, alias for sysctlmif_oidextendedbyname() */
int sysctlmif_oidinputbyname(const char *name, int *id, size_t *idlevel);


/* High-level API: functions related to `struct sysctlmif_object` */

SLIST_HEAD(sysctlmif_list, sysctlmif_object);
 /* compatibility <= 2.0.0 */
#define sysctlmif_object_list sysctlmif_list

struct sysctlmif_object {
	SLIST_ENTRY(sysctlmif_object) object_link;
	int      *id;            /* array of idlevel entries  */
	size_t   idlevel;        /* between 1 and CTL_MAXNAME */
	char     *name;          /* name in MIB notation      */
	char     *desc;          /* description               */
	char     *label;         /* aggregation label         */
	uint8_t  type;           /* defined in <sys/sysctl.h> */
	uint32_t flags;          /* defined in <sys/sysctl.h> */
	char     *fmt;           /* format string             */
	struct sysctlmif_list *children; /* children list     */
};

/* single object*/
struct sysctlmif_object *sysctlmif_object(int *id, size_t idlevel);
struct sysctlmif_object *sysctlmif_objectbyname(const char *name);
void sysctlmif_freeobject(struct sysctlmif_object *object);
/* list */
struct sysctlmif_list *sysctlmif_list();
struct sysctlmif_list *sysctlmif_grouplist(int *id, size_t idlevel);
struct sysctlmif_list *sysctlmif_grouplistbyname(const char *name);
struct sysctlmif_list *sysctlmif_leaves(int *id, size_t idlevel);
struct sysctlmif_list *sysctlmif_leavesbyname(const char *name);
void sysctlmif_freelist(struct sysctlmif_list *list);
/* tree */
struct sysctlmif_object *sysctlmif_tree(int *id, size_t idlevel);
struct sysctlmif_object *sysctlmif_treebyname(const char *name);
void sysctlmif_freetree(struct sysctlmif_object *object_root);
/* MIB */
struct sysctlmif_list *sysctlmif_mib();
void sysctlmif_freemib(struct sysctlmif_list *mib);

#endif /* _SYSCTLMIBINFO_H_ */
