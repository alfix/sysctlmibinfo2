# PUBLIC DOMAIN CC0 - NO WARRANTY
# <http://creativecommons.org/publicdomain/zero/1.0/>
#
# Written by Alfonso Sabato Siciliano

VERSION = 2.0.1
LIBRARY = sysctlmibinfo2
LIBRARY_SO = lib${LIBRARY:=.so}
LIBRARY_A = lib${LIBRARY:=.a}
OBJS = ${LIBRARY:=.o}
FBSDFLAGS= -O2 -pipe -std=gnu99 -Wno-format-zero-length \
	-fstack-protector-strong -Qunused-arguments
CFLAGS = -I/usr/local/include -fPIC -Wall -Wextra ${FBSDFLAGS}
LDFLAGS = -fstack-protector-strong -shared -Wl,-x -Wl,--fatal-warnings \
	-Wl,--warn-shared-textrel -Wl,-soname,${LIBRARY_SO}.${VERSION} 

INSTALL_PREFIX=/usr/local
AR = ar -crD
LN = ln -s
RM = rm -f
CP = cp
GZIP = gzip -cn
LDCONFIG = /sbin/ldconfig -m

all : man ${LIBRARY}

${LIBRARY}: ${OBJS}
	${CC} ${LDFLAGS} ${.ALLSRC} -o ${LIBRARY_SO}.${VERSION}
	${AR} ${LIBRARY_A} ${.ALLSRC}

.c.o:
	${CC} ${CFLAGS} -c ${.IMPSRC} -o ${.TARGET}

man:
	${GZIP} ${LIBRARY}.3 > ${LIBRARY}.3.gz

clean:
	${RM} ${LIBRARY_SO}* ${LIBRARY_A} *.o *~ *.gz

install:
	${CP} ${LIBRARY}.h ${INSTALL_PREFIX}/include
	${CP} ${LIBRARY_SO}.${VERSION} ${INSTALL_PREFIX}/lib/
	${LN} ${INSTALL_PREFIX}/lib/${LIBRARY_SO}.${VERSION} ${INSTALL_PREFIX}/lib/${LIBRARY_SO}
	${CP} ${LIBRARY_A} ${INSTALL_PREFIX}/lib/
	${LDCONFIG} ${INSTALL_PREFIX}/lib
	${CP} ${LIBRARY}.3.gz ${INSTALL_PREFIX}/man/man3/

unistall:
	${RM} ${INSTALL_PREFIX}/include/${LIBRARY}.h
	${RM} ${INSTALL_PREFIX}/lib/${LIBRARY_SO}
	${RM} ${INSTALL_PREFIX}/lib/${LIBRARY_SO}.${VERSION}
	${RM} ${INSTALL_PREFIX}/lib/${LIBRARY_A}
	${LDCONFIG} ${INSTALL_PREFIX}/lib
	${RM} ${INSTALL_PREFIX}/man/man3/${LIBRARY}.3.gz

